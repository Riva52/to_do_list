import React from 'react'

class AddTodo extends React.Component {
    render() {
        return (
            <div>
                <h1>hello</h1>
                <input type='text' ref='input' />
                <button onClick={(e) => this.handleClick(e)}>Add</button>
            </div>
        )
    }

    handleClick(e) {
        const node = this.refs.input
        const text = node.value.trim()
        this.props.onAddClick(text)
        node.value = ''
    }

}

export default AddTodo