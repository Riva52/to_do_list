export const ADD_TODO = 'ADD_TODO'

let nextTodoId = 0;

function addTodo(text) {
    return {
        type: ADD_TODO,
        id: nextTodoId++,
        text
    };
}

export default addTodo